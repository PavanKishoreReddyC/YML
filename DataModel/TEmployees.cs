﻿using System;
using System.Collections.Generic;

namespace DataModel
{
    public partial class TEmployees
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TextEncrypted { get; set; }
    }
}
