﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataModel
{
    public partial class legal_dbContext : DbContext
    {
        public virtual DbSet<ProductClassification> ProductClassification { get; set; }
        public virtual DbSet<TEmployees> TEmployees { get; set; }

        // Unable to generate entity type for table 'dsf.ShipmentLine_Mar2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Apr2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_May2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Jun2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Jul2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Aug2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Sep2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.median_test'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.dev_materials_median'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Jan2016'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.materials_final_median'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Feb2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_service_contract'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Mar2016'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.ewmd_only_users'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Apr2016'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.test'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_May2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.country'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Jun2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gm_sample_data'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.uniq_ec_svc'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Jul2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Cert_Data'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.ewmd_svc_merged'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Aug2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Sep2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HR_Extract'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Oct2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Nov2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Dec2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.gm_source_destination'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.global_acct'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Jan2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Feb2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.materials_counterfeit'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Mar2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.test_svc'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Apr2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_shipment_line1'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_May2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Jun2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Jul2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.drsp_domestic_diversion'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Aug2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.POS_DOMESTIC_DIVERSION'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ServiceContract_Sep2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.reseller_missing'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_s'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.new_domestic_reseller'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_details_dom'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_disti_stck'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_disti_drsp'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_dvar_drsp'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_details_bak'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.shipment_line_tmp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.service_contracts_tmp'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.country_theater_mapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.SO_Reseller_AM_2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.pos_tmp'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.SO_Reseller_AM_2017'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_orig_dest'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.lap_user'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_bookings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_bookings_am'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.lap_role'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_bookings_reseller'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.lap_report'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.bookings_am'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_new_partner_dom'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.drsp_joined_raw'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_new_partner_domestic'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.pos_joined_raw'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_dropship'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_new_partner_domestic1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.pos_tmp2'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_summary_bak'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_stocking'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_summary'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.gme_details'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.WAPP_11302017'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_pos'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_dest_orig'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.bookings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_shipment_hdr'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.bookings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ewmd_final'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_dest_orig1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Cty_Grouping'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.partner_analyzer_orig_dest1'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.svc_4ewmd'. Please see the warning messages.
        // Unable to generate entity type for table 'dev.country_region_mapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.raw_shipment_line'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.raw_shipment_line_tmp'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Jun2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Jul2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Aug2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Sep2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Oct2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Nov2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Dec2016'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Jan2017'. Please see the warning messages.
        // Unable to generate entity type for table 'wp.bookings_raw'. Please see the warning messages.
        // Unable to generate entity type for table 'dsf.ShipmentLine_Feb2017'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=p-lda-sqldb.cjppsdhni2np.us-west-2.rds.amazonaws.com,7899;User Id=legal_user;password=Sent1meThar#2;Database=legal_db;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductClassification>(entity =>
            {
                entity.HasKey(e => e.ProductId);

                entity.ToTable("Product_Classification");

                entity.Property(e => e.ProductId)
                    .HasColumnName("Product_ID")
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ProductDescription)
                    .HasColumnName("Product_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UsHts)
                    .HasColumnName("US_HTS")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ZCcats)
                    .HasColumnName("Z_CCATS")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ZEuEccn)
                    .HasColumnName("Z_EU_ECCN")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ZUsEccn)
                    .HasColumnName("Z_US_ECCN")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TEmployees>(entity =>
            {
                entity.ToTable("t_employees");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.TextEncrypted)
                    .HasColumnName("text_encrypted")
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });
        }
    }
}
