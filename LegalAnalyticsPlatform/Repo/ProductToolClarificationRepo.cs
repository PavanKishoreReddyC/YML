﻿using DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LegalAnalyticsPlatform.Repo
{
    public class ProductToolClarificationRepo
    {
        public static async Task<List<ProductClassification>> GetAllData()
        {
            using (var AppContext = new legal_dbContext())
            {
                return await AppContext.ProductClassification.AsNoTracking().ToListAsync();
            } 
        }

        public static async Task<List<ProductClassification>> GetAllData(string id)
        {
            using (var AppContext = new legal_dbContext())
            {
                var Ids = id.Split(",");
                //int[] ProductId = Ids.Select(x => Int32.Parse(x)).ToArray();
                var Data = await (from a in AppContext.ProductClassification
                                  where Ids.Contains(a.ProductId)
                                  select a).ToListAsync();
                return Data;

            }
        }
    }
}
