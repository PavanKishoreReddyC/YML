﻿class Home {
    static getList() {
        var d = new $.Deferred();

        Component.doGet('home').then(function (response) {
            d.resolve(response);
        }, function (error) {
            d.reject(error);
        });

        return d.promise();
    }

    static getById(productid) {
        var d = new $.Deferred();
        Component.doGet('home/' + productid).then(function (response) {
            d.resolve(response);
        }, function (error) {
            d.reject(error);
        });

        return d.promise();
    }

    static create(data) {
        var d = new $.Deferred();

        Component.doPost('home/create', data).then(function (response) {
            d.resolve(response);
        }, function (error) {
            d.reject(error);
        });

        return d.promise();
    }

    static update(id, data) {
        var d = new $.Deferred();

        Component.doPost('home/update/' + id, data).then(function (response) {
            d.resolve(response);
        }, function (error) {
            d.reject(error);
        });

        return d.promise();
    }

    static login(data) {
        var d = new $.Deferred();

        $.ajax({
            type: 'POST', url: myBaseUrl + 'api/home/login', data: JSON.stringify(data), dataType: 'json', contentType: "application/json",
            success: function (response) { Component.toggleLoading(); d.resolve(response); },
            error: function (error) { Component.toggleLoading(); d.reject(error); }
        });

        return d.promise();
    }

    static sendResetLink(data) {
        var d = new $.Deferred();

        Component.doPost('home/sendResetLink', data).then(function (response) {
            d.resolve(response);
        }, function (error) {
            d.reject(error);
        });

        return d.promise();
    }
}