﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel;
using LegalAnalyticsPlatform.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LegalAnalyticsPlatform.Api
{
    [Produces("application/json")]
    [Route("api/home")]
    public class HomeController : Controller
    {
        // GET: api/home
        [HttpGet]
        public async Task<List<ProductClassification>> getAllData()
        {
            return await ProductToolClarificationRepo.GetAllData();
        }

        // GET: api/home/5
        [HttpGet("{id}", Name = "GetAllProductClarificationDataById")]
        public async Task<List<ProductClassification>> getAllData([FromRoute]String id)
        {
            return await ProductToolClarificationRepo.GetAllData(id);
        }

        // POST: api/home
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/home/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
