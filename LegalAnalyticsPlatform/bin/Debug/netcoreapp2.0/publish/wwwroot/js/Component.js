﻿class Component {

    static doGet(url) {
        var d = new $.Deferred();

        $.ajax({
            type: 'GET', url: myBaseUrl + 'api/' + url, dataType: 'json', contentType: "application/json",
            success: function (response) { d.resolve(response); },
            error: function (error) { d.reject(error); },
        });

        return d.promise();
    }

    static doPost(url, data) {
        var d = new $.Deferred();

        $.ajax({
            type: 'POST', url: myBaseUrl + 'api/' + url, data: JSON.stringify(data), dataType: 'json', contentType: "application/json",
            success: function (response) { d.resolve(response); },
            error: function (error) { d.reject(error); },

        });

        return d.promise();
    }

    static doPut(url, data) {
        var d = new $.Deferred();

        $.ajax({
            type: 'PUT', url: myBaseUrl + 'api/' + url, data: JSON.stringify(data), dataType: 'json', contentType: "application/json",
            success: function (response) { d.resolve(response); },
            error: function (error) { d.reject(error); },

        });

        return d.promise();
    }

    static doDelete(url) {
        var d = new $.Deferred();

        $.ajax({
            type: 'DELETE', url: myBaseUrl + 'api/' + url, dataType: 'json', contentType: "application/json",
            success: function (response) { d.resolve(response); },
            error: function (error) { d.reject(error); },

        });

        return d.promise();
    }
    static handleApiError(error) {
        if (error.status == 401) { window.location.href = myBaseUrl + 'error/unauthorized'; }
        else { Component.showError(error.responseJSON); }
    }

    static redirect(url) {
        window.location.href = url;
    }

    static delayedRedirect(url) {
        setTimeout(function () { window.location.href = url }, 1000)
    }

    static goBack() {
        window.history.back();
    }
}
